"""
Telegram-botti, joka hakee basketball-reference-sivustolta tietoja edellisen yön NBA-kierroksesta ja lähettää
ne tilanneille käyttäjille.
Botin löytää telegramista käyttäjätunnuksella @nba_statsbot
"""


import requests
import datetime
from bs4 import BeautifulSoup

token = "928624171:AAHbDDSo8ME82aR5l5F2CuoDlnF0CVDMNQA"
now = datetime.datetime.now()
# TODO: Lisää kirjaimia
translationTable = str.maketrans("éàèùâêîôûçöäņģčć", "eaeuaeioucoangcc")

months = {"January": 1, "February": 2, "March": 3, "April": 4, "May": 5, "June": 6, "July": 7, "August": 8,
          "September": 9, "October": 10, "November": 11, "December": 12}

class BotHandler:

    def __init__(self, token):
        self.token = token
        self.api_url = "https://api.telegram.org/bot{}/".format(token)

    def get_updates(self, offset=None, timeout=30):
        method = 'getUpdates'
        params = {'timeout': timeout, 'offset': offset}
        resp = requests.get(self.api_url + method, params)
        result_json = resp.json()['result']
        return result_json

    def send_message(self, chat_id, text):
        params = {'chat_id': chat_id, 'text': text}
        method = 'sendMessage'
        resp = requests.post(self.api_url + method, params)
        return resp

    def get_last_update(self):
        get_result = self.get_updates()

        if len(get_result) > 0:
            last_update = get_result[-1]
        else:
            last_update = get_result[len(get_result)]

        return last_update

class NBA:
    """
    Etsii tiedot kaikista yön peleistä ja lisää ne dictiin.
    """

    def __init__(self, wanted_players_list, wanted_teams_list):
        self.__reference_url = "https://www.basketball-reference.com/boxscores/"   # Sivusto, jossa on kaikki pelit ja niiden tulokset
        self.__reference_request = requests.get(self.__reference_url)
        self.__soup = BeautifulSoup(self.__reference_request.text, features="html.parser")
        # Otsikot basic-boxscoressa. Ensimmäinen otsikko muutettu "Player"
        self.__basic_labels = {'Team':"", 'Player':"", 'MP':"", 'FG':"", 'FGA':"", 'FG%':"", '3P':"", '3PA':"",
                               '3P%':"", 'FT':"", 'FTA':"", 'FT%':"", 'ORB':"", 'DRB':"", 'TRB':"", 'AST':"",
                               'STL':"", 'BLK':"", 'TOV':"", 'PF':"", 'PTS':"", '+/-':""}

        # Joukkueiden koko nimet lyhenteiden perusteella.
        self.__team_labels = {'ATL':"Atlanta Hawks", 'BOS':"Boston Celtics", 'BRK':"Brooklyn Nets",
                              'CHA':"Charlotte Hornets", 'CHI':"Chicago Bulls", 'CLE':"Cleveland Cavaliers",
                              'DAL':"Dallas Mavericks", 'DEN':"Denver Nuggets", 'DET':"Detroit Pistons",
                              'GSW':"Golden State Warriors", 'HOU':"Houston Rockets", 'IND':"Indiana Pacers",
                              'LAC':"Los Angeles Clippers", 'LAL':"Los Angeles Lakers", 'MEM':"Memphis Grizzlies",
                              'MIA':"Miami Heat", 'MIL':"Milwaukee Bucks", 'MIN':"Minnesota Timberwolves",
                              'NOP': "New Orleans Pelicans", 'NYK':"New York Knicks", 'OKC':"Oklahoma City Thunder",
                              'ORL':"Orlando Magic",'PHI':"Philadelphia 76ers", 'PHO':"Phoenix Suns",
                              'POR':"Portland Trail Blazers", 'SAC':"Sacramento Kings", 'SAS':"San Antonio Spurs",
                              'TOR': "Toronto Raptors", 'UTA': "Utah Jazz", 'WAS': "Washington Wizards"}

        self.__wanted_players_list = wanted_players_list    # Lista pelaajista, joiden tiedot kiinnostavat
        self.__wanted_teams_list = wanted_teams_list        # Lista joukkueista, joiden tiedot kiinnostavat

        self.__game_url_list = []   # Lista tämän päivän pelien url-osoitteista

        self.__nba_stat_dict = {}   # Sanakirja, jossa nba-tilastot
        self.__nba_stat_list = []

        self.__nba_scores_dict = {} # Sanakirja, jossa nba-pelien tulokset

    def game_date(self):
        text = self.__soup.find("h1")
        date_list = text.get_text().split("on ")
        date = date_list[-1]
        return date

    def game_urls(self):
        for linkrow in self.__soup.find_all("a"):
            game_url_short = linkrow.get("href")        # Sivustolla pelin url-osoite on kerrottu ilman alkua
            if "boxscores/20" in game_url_short and "https://www.basketball-reference.com" + game_url_short not in self.__game_url_list:
                linkki = "https://www.basketball-reference.com" + game_url_short
                self.__game_url_list.append(linkki)

    def games(self):
        for game_url in self.__game_url_list:
            game_request = requests.get(game_url)
            game_soup = BeautifulSoup(game_request.text, features="html.parser")

            for scorebox in game_soup.find_all("div", {"class": "scorebox"}):   # Etsii pelin tuloslaatikon tilastosivulta
                game_team_names_list = []
                for team_name in scorebox.find_all("strong"):                   # Etsii joukkueiden nimet
                    team_name = team_name.get_text().strip("\n")
                    game_team_names_list.append(team_name)
                game = game_team_names_list[0] + " - " + game_team_names_list[1]
                self.__nba_scores_dict[game] = {}
                i = 0
                for score in game_soup.find_all("div", {"class": "score"}):     # Etsii joukkueiden tekemät pisteet
                    score = score.get_text()
                    self.__nba_scores_dict[game][game_team_names_list[i]] = score
                    i += 1

        return self.__nba_scores_dict

    def players(self):
        for game_url in self.__game_url_list:      # Käy jokaisen päivän pelin läpi
            game_request = requests.get(game_url)
            game_soup = BeautifulSoup(game_request.text, features="html.parser")
            #table_i = -2
            for table in game_soup.find_all("table"):  # Etsii kaikki taulukot ottelun tilastosivulta (yht. neljä kpl, advanced ja basic molemmille joukkueille)
                boxscore_id = table.get("id")  # boxscore_id sisältää onko taulukko basic ja kumpi joukkue. Esim: box_chi_basic

                if "basic" in boxscore_id and "game" in boxscore_id:  # Etsitään vain basic boxscoresta
                    #Joukkueen nimen hakeminen toteutettu team_labels dictin avulla.
                    for name_abbr in self.__team_labels:
                        if name_abbr in boxscore_id:
                            team_name = self.__team_labels[name_abbr]

                    for tbody in table.find_all("tbody"):  # tbody:ssä kaikki taulukon datarivit
                        for tr in tbody.find_all("tr"):  # tr:ssä taulukon rivi -> pelaaja
                            temp_stat_dict = {}
                            row = tr.get_text("|")
                            row_list = row.split("|")

                            # Rivin tietojen lisäys temp_stat_dictiin
                            stat_index = 0  # Käytetään siihen että tiedetään mikä sarake on kyseessä box scoresta (0 = nimi, 1 = peliaika jne)
                            stat_index_to_reduce = 0
                            # Jos mikä tahansa prosentti on tyhjä, niin kutsuttaessa row_list:ltä for-loopissa
                            # looppi ei käsittele heittoprosenttiruutua ollenkaan, jolloin se menisi yhden edellä.
                            # Tällöin indeksiä pitää muiden lukujen kohdalla vähentää.

                            if "MP" not in row_list and len(row_list) > 1:    # Ei haluta label-rivejä tarkasteluun

                                #Lisätään jokaiselle pelaajalle joukkue:
                                temp_stat_dict[list(self.__basic_labels.keys())[0]] = team_name

                                for stat in row_list:
                                    if stat_index == 0: # Ensimmäisenä tilastorivillä on nimi, lisätään se suoraan merkkijonona dictiin.
                                        temp_stat_dict[list(self.__basic_labels.keys())[stat_index+1]] = row_list[stat_index]
                                    elif stat_index == 1:   # Toisena on peliaika.
                                        if row_list[stat_index] == "Did Not Play":
                                            temp_stat_dict[list(self.__basic_labels.keys())[stat_index+1]] = row_list[stat_index]
                                            break
                                        else:
                                            temp_stat_dict[list(self.__basic_labels.keys())[stat_index+1]] = row_list[stat_index]
                                    else:   # Muut arvot ovat tilastoja, lisätään ne kokonaislukuina dictiin.
                                        if row_list[stat_index - 1] == "0" and stat_index == 4:   # Jos heittoyrityksiä ei ole, niin tilastorivillä heittoprosentin kohdalla on tyhjää.
                                            temp_stat_dict[list(self.__basic_labels.keys())[4+1]] = 0   # Tässä tapauksessa lisätään heittoprosentiksi 0.
                                            stat_index_to_reduce += 1
                                        elif row_list[stat_index - 1 - stat_index_to_reduce] == "0" and stat_index == 7:
                                            temp_stat_dict[list(self.__basic_labels.keys())[7+1]] = 0   # Sama kolmosheittoyrityksillä
                                            stat_index_to_reduce += 1
                                        elif row_list[stat_index - 1 - stat_index_to_reduce] == "0" and stat_index == 10:
                                            temp_stat_dict[list(self.__basic_labels.keys())[10+1]] = 0  # ja sama vapaaheittoyrityksillä
                                            stat_index_to_reduce += 1
                                            #TODO: toimii, saako for-loopin sijaan tehtyä eri tavalla?
                                        elif stat_index == 19 and stat_index_to_reduce == 1:
                                            temp_stat_dict[list(self.__basic_labels.keys())[stat_index+1]] = row_list[stat_index - stat_index_to_reduce]
                                            temp_stat_dict[list(self.__basic_labels.keys())[-1]] = row_list[-1]
                                        elif stat_index == 18 and stat_index_to_reduce == 2:
                                            temp_stat_dict[list(self.__basic_labels.keys())[stat_index+1]] = row_list[stat_index - stat_index_to_reduce]
                                            temp_stat_dict[list(self.__basic_labels.keys())[-2]] = row_list[-2]
                                            temp_stat_dict[list(self.__basic_labels.keys())[-1]] = row_list[-1]
                                        elif stat_index == 16 and stat_index_to_reduce == 3:
                                            temp_stat_dict[list(self.__basic_labels.keys())[stat_index+1]] = row_list[stat_index - stat_index_to_reduce]
                                            temp_stat_dict[list(self.__basic_labels.keys())[-4]] = row_list[-4]
                                            temp_stat_dict[list(self.__basic_labels.keys())[-3]] = row_list[-3]
                                            temp_stat_dict[list(self.__basic_labels.keys())[-2]] = row_list[-2]
                                            temp_stat_dict[list(self.__basic_labels.keys())[-1]] = row_list[-1]
                                        else:   # Muissa tapauksissa lisätään suoraan ruudun luku
                                            temp_stat_dict[list(self.__basic_labels.keys())[stat_index+1]] = row_list[stat_index - stat_index_to_reduce]

                                    stat_index += 1
                                if len(temp_stat_dict) > 15:
                                    self.__nba_stat_list.append(temp_stat_dict)

        return self.__nba_stat_list

    def is_team_name_ok(self, text):
        if text in self.__team_labels or text in self.__team_labels.values():
            return True
        else:
            return False

    def get_team_name(self, abbr):
        return self.__team_labels[abbr]


def info_to_print(wanted_info_list, player_statrow):
    # Muut mahdolliset tilastot puuttuu vielä
    mp = ""
    fg = ""
    tp = ""
    ft = ""
    rb = ""
    ast = ""
    stl = ""
    blk = ""
    tov = ""
    pts = ""
    pm = ""
    if "MP" in wanted_info_list:
        mp = "MP: " + str(player_statrow["MP"]) + ", "
    if "FG" in wanted_info_list:
        fg = "FG: " + str(player_statrow["FG"]) + "/" + str(player_statrow["FGA"]) + ", "
    if "3P" in wanted_info_list:
        tp = "3P: " + str(player_statrow["3P"]) + "/" + str(player_statrow["3PA"]) + ", "
    if "FT" in wanted_info_list:
        ft = "FT: " + str(player_statrow["FT"]) + "/" + str(player_statrow["FTA"]) + ", "
    if "TRB" in wanted_info_list:
        rb = "REB: " + str(player_statrow["TRB"]) + ", "
    if "AST" in wanted_info_list:
        ast = "AST: " + str(player_statrow["AST"]) + ", "
    if "STL" in wanted_info_list:
        stl = "STL: " + str(player_statrow["STL"]) + ", "
    if "BLK" in wanted_info_list:
        blk = "BLK: " + str(player_statrow["BLK"]) + ", "
    if "TOV" in wanted_info_list:
        tov = "TOV: " + str(player_statrow["TOV"]) + ", "
    if "PTS" in wanted_info_list and "PTS" in player_statrow:
        pts = "PTS: " + str(player_statrow["PTS"])
    #TODO: Ei toimi
    #if "+/-" in wanted_info_list:
        #pm = "+/-: " + str(player_statrow["+/-"])

    player_statrow_str = player_statrow["Player"] + ":\n  " + mp + fg + tp + ft + rb + ast + stl + blk + tov + pts
    return player_statrow_str

def is_statrow_dd(player_statrow_dict):
    stats_over_10 = 0
    for stat_key in player_statrow_dict:
        if (stat_key == "TRB" or stat_key == "AST" or stat_key == "STL" or stat_key == "BLK" or stat_key == "PTS") \
                and len(player_statrow_dict[stat_key]) == 2:
            stats_over_10 += 1
    if stats_over_10 == 2:
        return True
    return False

def is_statrow_td(player_statrow_dict):
    stats_over_10 = 0
    for stat_key in player_statrow_dict:
        if (stat_key == "TRB" or stat_key == "AST" or stat_key == "STL" or stat_key == "BLK" or stat_key == "PTS") \
                and len(player_statrow_dict[stat_key]) == 2:
            stats_over_10 += 1
    if stats_over_10 == 3:
        return True
    return False


def data(wanted_players_list, wanted_teams_list, nba_scores_dict, nba_playerstats_list, wanted_info_list,
         other_wanted_statrows_dict):

    data_string = ""
    already_printed_players_list = []

    # Erikoistilastorivien etsiminen
    special_stat_lines_list = []
    # Ne kiinnostavat pelaajat, joilla on ollut peli, täytyy etsiä erikseen niistä, jotka eivät ole pelanneet.
    wanted_players_played_list = []
    for player_statrow_dict in nba_playerstats_list:
        if is_statrow_td(player_statrow_dict) and other_wanted_statrows_dict["TD"] == True:
            special_stat_lines_list.append(player_statrow_dict)
        elif is_statrow_dd(player_statrow_dict) and other_wanted_statrows_dict["DD"] == True:
            special_stat_lines_list.append(player_statrow_dict)
        elif int(player_statrow_dict["PTS"]) >= other_wanted_statrows_dict["PTS"]\
                or int(player_statrow_dict["3P"]) >= other_wanted_statrows_dict["3P"]\
                or int(player_statrow_dict["TRB"]) >= other_wanted_statrows_dict["TRB"]\
                or int(player_statrow_dict["AST"]) >= other_wanted_statrows_dict["AST"]\
                or int(player_statrow_dict["BLK"]) >= other_wanted_statrows_dict["BLK"]\
                or int(player_statrow_dict["STL"]) >= other_wanted_statrows_dict["STL"]:
            special_stat_lines_list.append(player_statrow_dict)

        if player_statrow_dict["Player"] in wanted_players_list:
            wanted_players_played_list.append(player_statrow_dict["Player"])


    for game in nba_scores_dict:

        # Pelin tuloksen tulostus
        team_index = 0
        team_a = ""
        team_b = ""
        for team in nba_scores_dict[game]:
            if team_index == 0:
                team_a = team
            elif team_index == 1:
                team_b = team
            team_index += 1
        data_string += ("\n\n" + game + ": " + nba_scores_dict[game][team_a] + " - " + nba_scores_dict[game][team_b])

        # Kaikkien pelaajien tulostus jos joukkue halutuissa joukkueissa
        if team_a in wanted_teams_list:
            data_string += ("\n" + team_a + " stats:")
            for player_statrow_dict in nba_playerstats_list:
                if player_statrow_dict["Team"] == team_a:
                    player_statrow_str = info_to_print(wanted_info_list, player_statrow_dict)
                    data_string += ("\n" + player_statrow_str)
                    already_printed_players_list.append(player_statrow_dict["Player"])

        if team_b in wanted_teams_list:
            data_string += (team_b + " stats:")
            for player_statrow_dict in nba_playerstats_list:
                if player_statrow_dict["Team"] == team_b:
                    player_statrow_str = info_to_print(wanted_info_list, player_statrow_dict)
                    data_string += ("\n" + player_statrow_str)
                    already_printed_players_list.append(player_statrow_dict["Player"])

        label_printed = False
        for player_statrow_dict in special_stat_lines_list:
            if player_statrow_dict["Player"] not in already_printed_players_list and \
                    (team_a == player_statrow_dict["Team"] or team_b == player_statrow_dict["Team"]):
                if not label_printed:
                    data_string += "\nSpecial statlines:"
                    label_printed = True
                player_statrow_str = info_to_print(wanted_info_list, player_statrow_dict)
                data_string += ("\n" + player_statrow_str)
                already_printed_players_list.append(player_statrow_dict["Player"])

        # Kiinnostavaksi kerrottujen pelaajien tilastorivit
        # Jos kiinnostava pelaaja on jo printattu, niin se poistetaan halutuista pelaajista
        for player in wanted_players_list:
            if player in already_printed_players_list and player in wanted_players_played_list:
                wanted_players_played_list.remove(player)

        label_printed = False
        if len(wanted_players_played_list) > 0:
            for player_statrow_dict in nba_playerstats_list:
                player = player_statrow_dict["Player"]
                player_translated = player.translate(translationTable)
                if (player in wanted_players_played_list or player_translated in wanted_players_played_list)and \
                        (team_a == player_statrow_dict["Team"] or team_b == player_statrow_dict["Team"]):
                    if not label_printed:
                        data_string += ("\nOther interesting players:")
                        label_printed = True
                    player_statrow_str = info_to_print(wanted_info_list, player_statrow_dict)
                    data_string += ("\n" + player_statrow_str)
                    already_printed_players_list.append(player_statrow_dict["Player"])

    return data_string

def bot_commands(nba_creature, stats_bot, last_chat_text, last_chat_id, chat_ids, wanted_players_list,
                 wanted_teams_list):
    primary_text = last_chat_text
    primary_id = last_chat_id

    # Tehdään toinen lista halutuista pelaajista, jossa nimet on ilman ulkomaalaisia kirjaimia.
    wanted_players_translated_list = []
    for player in wanted_players_list:
        player_translated = player.translate(translationTable)
        wanted_players_translated_list.append(player_translated)

    if last_chat_text == "/subscribe" and last_chat_id not in chat_ids:
        chat_ids.append(last_chat_id)
        stats_bot.send_message(last_chat_id, "Subscribed")
    if last_chat_text == "/unsubscribe" and last_chat_id in chat_ids:
        chat_ids.remove(last_chat_id)
        stats_bot.send_message(last_chat_id, "Unsubscribed")

    if last_chat_text == "/addplayer":
        stats_bot.send_message(last_chat_id, "Give the player's name")

        # Odottaa vastausta ja lisää pelaajan haluttuihin pelaajiin.
        is_answered = False
        while not is_answered:
            last_update = stats_bot.get_last_update()
            last_chat_text = last_update['message']['text']
            last_chat_id = last_update['message']['chat']['id']
            # Testataan, että käyttäjä on lähettänyt uuden viestin,
            if last_chat_text != primary_text and last_chat_id == primary_id:
                last_chat_text_translated = last_chat_text.translate(translationTable)
                if last_chat_text in wanted_players_list or last_chat_text_translated in wanted_players_list\
                        or last_chat_text in wanted_players_translated_list:
                    is_answered = True
                    stats_bot.send_message(last_chat_id, "Player already on the list")
                    # TODO: ? Testaus, onko nimi oikein -> vaatii nimilistan
                else:
                    wanted_players_list.append(last_chat_text)
                    stats_bot.send_message(last_chat_id, last_chat_text + " added")
                    is_answered = True

    if last_chat_text == "/removeplayer":

        stats_bot.send_message(last_chat_id, "Give the player's name")
        # Odottaa vastausta ja poistaa pelaajan halutuista pelaajista.
        is_answered = False
        while not is_answered:
            last_update = stats_bot.get_last_update()
            last_chat_text = last_update['message']['text']
            last_chat_id = last_update['message']['chat']['id']

            last_chat_text_translated = last_chat_text.translate(translationTable)
            # Testataan, että sama käyttäjä on lähettänyt uuden viestin:
            if last_chat_text != primary_text and last_chat_id == primary_id:
                if last_chat_text not in wanted_players_list and last_chat_text_translated not in wanted_players_translated_list:
                    is_answered = True
                    stats_bot.send_message(last_chat_id, "Player not on the list")
                else:
                    # Jos pelaajan nimi annettu suoraan sellaisena kuin se on oikeasti:
                    try:
                        is_answered = True
                        wanted_players_list.remove(last_chat_text)
                        stats_bot.send_message(last_chat_id, last_chat_text + " removed")
                    # Jos pelaajan nimi annettu ilman sen vieraskielisiä kirjaimia:
                    except ValueError:
                        is_answered = True
                        player_index = wanted_players_translated_list.index(last_chat_text_translated)
                        stats_bot.send_message(last_chat_id, wanted_players_list[player_index] + " removed")
                        wanted_players_translated_list.remove(last_chat_text_translated)
                        del wanted_players_list[player_index]

    if last_chat_text == "/listplayers":
        players_str = ""
        for player in wanted_players_list:
            if not players_str:
                players_str += player
            else:
                players_str += (", " + player)
        stats_bot.send_message(last_chat_id, players_str)

    if last_chat_text == "/addteam":
        stats_bot.send_message(last_chat_id, "Give the team name or abbreviation")

        # Odottaa vastausta ja lisää pelaajan haluttuihin joukkueisiin.
        is_answered = False
        while not is_answered:
            last_update = stats_bot.get_last_update()
            last_chat_text = last_update['message']['text']
            last_chat_id = last_update['message']['chat']['id']
            # Testataan, että käyttäjä on lähettänyt uuden viestin:
            if last_chat_text != primary_text and last_chat_id == primary_id:
                # Annettu joukkue jo halutuissa joukkueissa:
                if last_chat_text in wanted_teams_list: # or nba_creature.get_team_name(last_chat_text) in wanted_teams_list:
                    is_answered = True
                    stats_bot.send_message(last_chat_id, "Team already on the list")
                # Joukkueen nimi virheellinen (testaa myös lyhenteen mukaan):
                elif not nba_creature.is_team_name_ok(last_chat_text):
                    is_answered = True
                    stats_bot.send_message(last_chat_id, "Team does not exist")
                # Joukkueen voi lisätä joukkueen nimen tai lyhenteen perusteella.
                # Lyhenteenkin perusteella lisätyt tulevat listaan joukkueen koko nimellä.
                else:
                    is_answered = True
                    try:
                        team_name = nba_creature.get_team_name(last_chat_text)
                        if team_name in wanted_teams_list:
                            stats_bot.send_message(last_chat_id, "Team already on the list")
                        else:
                            wanted_teams_list.append(team_name)
                            stats_bot.send_message(last_chat_id, team_name + " added")
                    except:
                        stats_bot.send_message(last_chat_id, "Team does not exist")

    if last_chat_text == "/removeteam":
        stats_bot.send_message(last_chat_id, "Give the team name or abbreviation")
        # Odottaa vastausta ja poistaa joukkueen halutuista pelaajista.
        is_answered = False
        while not is_answered:
            last_update = stats_bot.get_last_update()
            last_chat_text = last_update['message']['text']
            last_chat_id = last_update['message']['chat']['id']
            # Testataan, että sama käyttäjä on lähettänyt uuden viestin:
            if last_chat_text != primary_text and last_chat_id == primary_id:
                is_answered = True
                # Jos käyttäjä haluaa poistaa joukkueen lyhenteen perusteella:
                if len(last_chat_text) == 3:
                    try:
                        team_name = nba_creature.get_team_name(last_chat_text)
                        wanted_teams_list.remove(team_name)
                        stats_bot.send_message(last_chat_id, team_name + " removed")
                    except:
                        stats_bot.send_message(last_chat_id, "Team not on the list")
                # Jos joukkue poistetaan sen koko nimellä, mutta nimi on väärä
                elif last_chat_text not in wanted_teams_list:
                    stats_bot.send_message(last_chat_id, "Team not on the list")
                # Jos joukkue poistetaan sen koko nimellä ja poistaminen onnistuu:
                else:
                    wanted_teams_list.remove(last_chat_text)
                    stats_bot.send_message(last_chat_id, last_chat_text + " removed")


    # Listaa tällä hetkellä kiinnostavissa joukkueissa olevat
    if last_chat_text == "/listteams":
        teams_str = ""
        for team in wanted_teams_list:
            # Eri muotoilu ensimmäiselle joukkueelle ja muille
            if not teams_str:
                teams_str += team
            else:
                teams_str += (", " + team)
        stats_bot.send_message(last_chat_id, teams_str)

def main():

    # Miltä pelaajilta halutaan kaikki wanted_info_list sisältävät tiedot
    wanted_players_list = ["Lauri Markkanen", "Danny Green", "Luka Dončić", "Brandon Ingram"]

    # Miltä joukkueilta halutaan kaikki wanted_info_list sisältävät tiedot
    wanted_teams_list = ["Los Angeles Lakers"]

    # Mitä kiinnostavien joukkueiden kaikilta pelaajilta kerrotaan
    # Myöhemmin boolean dict?
    wanted_info_list = ["Player", "FG", "3P", "TRB", "AST", "STL", "BLK", "PTS"]

    # Muunlaiset tilastorivit, jotka voivat olla haluttuja, ja niiden raja-arvot
    other_wanted_statrows_dict = {"DD": False, "TD": True, "PTS": 35, "3P": 8, "TRB": 19, "AST": 15, "BLK": 7, "STL": 7}

    nba_creature = NBA(wanted_players_list, wanted_teams_list)

    while True:

        # Uusimpien sivuston tietojen päivämäärä
        game_date_str = nba_creature.game_date()
        game_date_list = game_date_str.split(" ")
        game_month = game_date_list[0]
        game_month_num = months[game_month]
        game_day = game_date_list[1].strip(",")

        nba_creature.game_urls()

        nba_scores_dict = nba_creature.games()
        nba_playerstats_list = nba_creature.players()

        data_string = data(wanted_players_list, wanted_teams_list, nba_scores_dict, nba_playerstats_list, wanted_info_list,
             other_wanted_statrows_dict)

        print("Data collecting done on " + game_date_str)

        # TODO: Turhia ?
        new_offset = None
        month_name = now.strftime("%B")
        day = now.day
        hour = now.hour
        minute = now.minute

        chat_ids = []

        stats_printed = False
        previous_update_id = 0
        while True:
            new_game_date_str = nba_creature.game_date()
            new_game_date_list = game_date_str.split(" ")
            new_game_month = game_date_list[0]
            new_game_month_num = months[game_month]
            new_game_day = game_date_list[1].strip(",")


            stats_bot = BotHandler(token)

            stats_bot.get_updates(new_offset)

            last_update = stats_bot.get_last_update()

            last_update_id = last_update['update_id']
            last_chat_text = last_update['message']['text']
            last_chat_id = last_update['message']['chat']['id']
            last_chat_name = last_update['message']['chat']['first_name']

            # tilaajien chat_id:t, käyttäjät joille tilastot lähetetään

            if previous_update_id != last_update_id:
                bot_commands(nba_creature, stats_bot, last_chat_text, last_chat_id, chat_ids, wanted_players_list,
                            wanted_teams_list)

            if not stats_printed and chat_ids:
                stats_printed = True
                for user in chat_ids:
                    stats_bot.send_message(user, data_string)

            previous_update_id = last_update_id

            # Jos sivustolle on tullut uuden pelipäivän tiedot, niin ne haetaan uudelleen ja lähetetään
            # tilanneille käyttäjille.
            if (new_game_day > game_day and game_month_num == new_game_month_num) or \
                    (new_game_day < game_day and game_month_num + 1 == new_game_month_num):
                break



if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        exit()